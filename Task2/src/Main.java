import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application{
	final static String DEFAULT_CLUSTER_FILE_LOCATION = "Cluster.txt";
    
	public static void main(String[] args) {
        launch();
    }
	
	
	public static void printDataPoints(ArrayList<DataPoint> dataPoints) {
    	for(DataPoint dataPoint : dataPoints) {
    		System.out.println("X: " + dataPoint.getxCoordinate());
    		System.out.println("Y: " + dataPoint.getyCoordinate());
    		System.out.println("Cluster: " + dataPoint.getCluster());
    	}
	}
    

	public static ArrayList<DataPoint> importDataPointsFromFile(String clusterFileLocation){
		ArrayList<DataPoint> results = new ArrayList<DataPoint>();
		try {
			File file = new File(clusterFileLocation);
			Scanner input = new Scanner(file);
			int lineCount = 0;
			while(input.hasNextLine()) {
				// Create an instance of the TaxRate class for each of the rows in the file
				String line = input.nextLine();
				if(lineCount > 0) {
					String[] lineSplit = line.split("\t");					
					DataPoint dataPoint = new DataPoint(Double.parseDouble(lineSplit[0]), Double.parseDouble(lineSplit[1]), lineSplit[2]);
					results.add(dataPoint);
				}
				lineCount++;
			}
			input.close();
		} catch (FileNotFoundException e) {
			String newLocation = promptForNewFileLocation(clusterFileLocation);
			results = importDataPointsFromFile(newLocation);
		}
		return results;
	}
	
	
	public static String promptForNewFileLocation(String fileLocation) {
		// If application is unable to locate default text file, user will be prompted to input the location of the file
		Scanner userInput = new Scanner(System.in);
		System.out.println("Unable to locate " + fileLocation);
		System.out.print("Please enter the location of the cluster file: ");
		String inputFileLocation = userInput.next();
		
		// Tax rates file name variable check to confirm that it has txt on the end
		if(!inputFileLocation.contains(".txt")) {
			System.out.println("Please enter the path with .txt on the end");
			inputFileLocation = promptForNewFileLocation(inputFileLocation);
		}
		userInput.close();
		return inputFileLocation;
	}    
    
    
    @Override
    public void start(Stage stage) {
    	ArrayList<DataPoint> importedDataPoints = importDataPointsFromFile(DEFAULT_CLUSTER_FILE_LOCATION);
    	
    	// Defining the X axis
    	NumberAxis xAxis = new NumberAxis(0, 8, 2);
    	xAxis.setLabel("X Axis");
    	
    	NumberAxis yAxis = new NumberAxis(0, 10, 1);	
    	yAxis.setLabel("Y Axis");
    	
    	// Creating the scatter chart
    	ScatterChart<Number, Number> scatterChart = new ScatterChart<Number, Number>(xAxis, yAxis);
    	
    	// Prepare the data
    	XYChart.Series<Number, Number> series1 = new XYChart.Series<>();
    	series1.setName("Cluster1");
    	XYChart.Series<Number, Number> series2 = new XYChart.Series<>();
    	series2.setName("Cluster2");
    	XYChart.Series<Number, Number> series3 = new XYChart.Series<>();
    	series3.setName("Cluster3");
    	XYChart.Series<Number, Number> series4 = new XYChart.Series<>();
    	series4.setName("Cluster4");
    	
    	for(DataPoint dataPoint : importedDataPoints) {
    		double x = dataPoint.getxCoordinate();
    		double y = dataPoint.getyCoordinate();
    		switch(dataPoint.getCluster()) {
    			case "Cluster1":
    				series1.getData().add(new XYChart.Data<Number, Number>(x, y));
    				break;
    			case "Cluster2":
    				series2.getData().add(new XYChart.Data<Number, Number>(x, y));
    				break;
    			case "Cluster3":
    				series3.getData().add(new XYChart.Data<Number, Number>(x, y));
    				break;
    			case "Cluster4":
    				series4.getData().add(new XYChart.Data<Number, Number>(x, y));
    				break;
    		}
    	}
    	
    	// Add data to scatter chart
    	scatterChart.getData().addAll(series1);
    	scatterChart.getData().addAll(series2);
    	scatterChart.getData().addAll(series3);
    	scatterChart.getData().addAll(series4);
    	
    	// Needs to be in the start method
    	Group root = new Group(scatterChart);
    	
    	// Creating a scene
    	Scene scene = new Scene(root, 600, 600);
    	scene.setFill(null);
    	
    	// Setting the title of the stage
    	stage.setTitle("Scatter Graph");
    	
    	// Adding the scene to the stage
    	stage.setScene(scene);
    	
    	// Displaying the contents of the stage
    	stage.show();
    	
    }
}
