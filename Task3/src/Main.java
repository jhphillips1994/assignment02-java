import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.stage.Stage;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;


public class Main extends Application {
    private javafx.event.EventHandler<ActionEvent> ActionEvent;
  
	  protected BorderPane getPane() {
	    HBox paneForButtons = new HBox(20);
	    RadioButton rbRed = new RadioButton("Red");
	    RadioButton rbYellow = new RadioButton("Yellow");
	    RadioButton rbGreen = new RadioButton("Green");
	    
	    ToggleGroup group = new ToggleGroup();
	    rbRed.setToggleGroup(group);
	    rbYellow.setToggleGroup(group);
	    rbGreen.setToggleGroup(group);
	    
	    
	    paneForButtons.getChildren().addAll(rbRed, rbYellow, rbGreen);
	    paneForButtons.setAlignment(Pos.CENTER);
	
	    BorderPane pane = new BorderPane();
	    pane.setBottom(paneForButtons);
	    
	    // Handles Rectangle
	    Rectangle borderRectangle = new Rectangle();
	    borderRectangle.setX(175);
	    borderRectangle.setY(25);
	    borderRectangle.setWidth(100);
	    borderRectangle.setHeight(230);
	    borderRectangle.setFill(Color.WHITE);
	    borderRectangle.setStroke(Color.BLACK);
	    
	    // Handles Circles
        Circle circleRed = new Circle();
        circleRed.setCenterX(225);
        circleRed.setCenterY(70);
        circleRed.setRadius(20);
        circleRed.setStroke(Color.BLACK);
        circleRed.setFill(Color.WHITE);
        
        Circle circleYellow = new Circle();
        circleYellow.setCenterX(225);
        circleYellow.setCenterY(140);
        circleYellow.setRadius(20);
        circleYellow.setStroke(Color.BLACK);
        circleYellow.setFill(Color.WHITE);
        
        Circle circleGreen = new Circle();
        circleGreen.setCenterX(225);
        circleGreen.setCenterY(210);
        circleGreen.setRadius(20);
        circleGreen.setStroke(Color.BLACK);
        circleGreen.setFill(Color.WHITE);
        
        
        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
        	public void changed(ObservableValue<? extends Toggle> ob, Toggle o, Toggle n){
                RadioButton rb = (RadioButton)group.getSelectedToggle();
                String selectedRadioButton = rb.getText();
                switch(selectedRadioButton) {
                	case "Red":
                		circleRed.setFill(Color.RED);
                		circleYellow.setFill(Color.WHITE);
                		circleGreen.setFill(Color.WHITE);
                		break;
                	case "Yellow":
                		circleRed.setFill(Color.WHITE);
                		circleYellow.setFill(Color.YELLOW);
                		circleGreen.setFill(Color.WHITE);
                		break;
                	case "Green":
                		circleRed.setFill(Color.WHITE);
                		circleYellow.setFill(Color.WHITE);
                		circleGreen.setFill(Color.GREEN);
                		break;
                }
        	}
        	
        });
        

        // Create a pane to hold the circle 
	    pane.getChildren().add(borderRectangle);
	    pane.getChildren().add(circleRed);
        pane.getChildren().add(circleYellow);
        pane.getChildren().add(circleGreen);
    
	    return pane;
	  }
	  

	  @Override // Override the start method in the Application class
	  public void start(Stage primaryStage) {
	    // Create a scene and place it in the stage
	    Scene scene = new Scene(getPane(), 450, 300);
	    primaryStage.setTitle("Traffic Light Simulation");
	    primaryStage.setScene(scene);
	    primaryStage.show();
	  }
	
	  
	  public static void main(String[] args) {
	    launch(args);
	  }

}
