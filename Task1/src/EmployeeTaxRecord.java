import java.math.BigDecimal;

public class EmployeeTaxRecord {
	
	private int employeeId;
	private BigDecimal taxableIncome;
	private BigDecimal tax;
	
	
	public EmployeeTaxRecord () {
		this.employeeId = 0;
		this.taxableIncome = BigDecimal.valueOf(0);
		this.tax = BigDecimal.valueOf(0);
	}
	
	public EmployeeTaxRecord (int employeeId) {
		this.employeeId = employeeId;
		this.taxableIncome = BigDecimal.valueOf(0);
		this.tax = BigDecimal.valueOf(0);				
	}
	
	public EmployeeTaxRecord (int employeeId, BigDecimal taxableIncome, BigDecimal tax) {
		this.employeeId = employeeId;
		this.taxableIncome = taxableIncome;
		this.tax = tax;				
	}
	
	public int getEmployeeId() {
		return employeeId;
	}
	
	
	public BigDecimal getTaxableIncome() {
		return taxableIncome;
	}
	
	
	public BigDecimal getTax() {
		return tax;
	}
	
	
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	
	
	public void setTaxableIncome(BigDecimal taxableIncome) {
		this.taxableIncome = taxableIncome;
	}
		
	
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
}
