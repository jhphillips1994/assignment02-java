import java.util.ArrayList;
import java.util.InputMismatchException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Scanner;
import java.io.File;

public class Main {
	
	static String taxRatesFileLocation = "taxrates.txt";
	static String taxReportFileLocation = "taxreport.txt";
	
	
	public static void main(String[] args) {
		// Add the tax total details to the array list with the employee tax record details
		Scanner userInput = new Scanner(System.in);
		runMenu(userInput);
		userInput.close();
	}
	
	public static void runMenu(Scanner userInput) {
		int menuSelection = 0;
		while(menuSelection < 1 || menuSelection > 3) {
			displayMenu();
			try {
				menuSelection = userInput.nextInt();
			} catch(InputMismatchException e) {
				System.out.println("\nError: A String was entered");
				userInput.nextLine();
				runMenu(userInput);
			}
		}	
		
		switch(menuSelection) {
			case 1:
				// Import employee tax records and store them within an Array list
				ArrayList<EmployeeTaxRecord> employeeTaxRecordsToCheck = getDataFromFile(taxReportFileLocation);
				// Import tax rates and store them within an Array List
				ArrayList<TaxRate> taxRates = getDataFromFile(taxRatesFileLocation);
				getNewEmployeeTaxDetails(userInput, taxRates, employeeTaxRecordsToCheck);
				break;
			case 2:
				// Import employee tax records and store them within an Array list
				ArrayList<EmployeeTaxRecord> employeeTaxRecords = getDataFromFile(taxReportFileLocation);
				searchTaxReportForEmployeeTaxRecord(employeeTaxRecords, userInput);
				runMenu(userInput);
				break;
			case 3:
				System.exit(0);
				break;
		}
	}
	
	public static void searchTaxReportForEmployeeTaxRecord(ArrayList<EmployeeTaxRecord> employeeTaxRecords, Scanner userInput) {
		int employeeId = promptForEmployeeId(userInput);
		EmployeeTaxRecord employeeTaxRecord = searchEmployeeTaxRecords(employeeId, employeeTaxRecords);
		if(employeeTaxRecord.getEmployeeId() == 0) {
			System.out.println("Taxreport file does not contain an employee with the ID: " + employeeId);
		} else {
			printEmployeeTaxRecord(employeeTaxRecord);	
		}
		String userResponse = "";
		while(!userResponse.equals("1") || !userResponse.equals("2")) {
			System.out.println("Would you like to search for another employee?");
			System.out.println("1. Search for another employee");
			System.out.println("2. Return to main menu");
			userResponse = userInput.next();
			if(userResponse.equals("1")) {
				searchTaxReportForEmployeeTaxRecord(employeeTaxRecords, userInput);
			} else if (userResponse.equals("2")){
				runMenu(userInput);
			}
		}
	}
	
	
	public static int promptForEmployeeId(Scanner userInput) {
		String employeeId = "";
		while(employeeId.length() != 4 ) {
			System.out.println("Please enter a 4 digit employee ID:");
			employeeId = userInput.next();
		}
		return Integer.parseInt(employeeId);
	}
	
	
	public static EmployeeTaxRecord searchEmployeeTaxRecords(int employeeId, ArrayList<EmployeeTaxRecord> employeeTaxRecords) {
		EmployeeTaxRecord result = new EmployeeTaxRecord();
		for(EmployeeTaxRecord employeeTaxRecord : employeeTaxRecords) {
			if(employeeTaxRecord.getEmployeeId() == employeeId) {
				result = employeeTaxRecord;
			}
		}
		return result;
	}
	
	
	public static void getNewEmployeeTaxDetails(Scanner userInput, ArrayList<TaxRate> taxRates, ArrayList<EmployeeTaxRecord> employeeTaxRecordsToCheck) {
		String employeeId = "";
		while(employeeId.length() != 4 ) {
			System.out.println("\nPlease enter a 4 digit employee ID:");
			employeeId = userInput.next();
		}
		// Search in that checks if the employee ID entered is already within the report file
		boolean employeeAlreadyExists = checkIfEmployeeAlreadyExists(employeeId, employeeTaxRecordsToCheck);
		if(employeeAlreadyExists) {
			System.out.println("\nEmployee already exists in tax report file");
			promptUsertoAddAnotherEmployee(userInput, taxRates, employeeTaxRecordsToCheck);
		}
		String annualIncomeInput = "";
		BigDecimal annualIncome = BigDecimal.valueOf(0);
		System.out.println("\nPlease enter the annual salary of the employee");
		while(true) {
			System.out.println("Please enter a value in the following format: 1000.00");
			annualIncomeInput = userInput.next();
			try {
				annualIncome = new BigDecimal(annualIncomeInput);
				annualIncome = annualIncome.setScale(2);
				break;
			} catch (NumberFormatException e) {
				System.out.println("The value entered did not match the required input");
			}
		}
		BigDecimal tax = calculateTax(taxRates, annualIncome.doubleValue());
		tax = tax.setScale(2);
		EmployeeTaxRecord employeeTaxRecord = new EmployeeTaxRecord(Integer.parseInt(employeeId), annualIncome, tax);
		// Write record to file
		writeEmployeeTaxRecordToFile(employeeTaxRecord, taxReportFileLocation);
		promptUsertoAddAnotherEmployee(userInput, taxRates, employeeTaxRecordsToCheck);
	}
	
	
	public static boolean checkIfEmployeeAlreadyExists(String employeeId, ArrayList<EmployeeTaxRecord> employeeTaxRecordsToCheck) {
		boolean result = false;
		for(EmployeeTaxRecord employeeTaxRecord : employeeTaxRecordsToCheck) {
			if(Integer.parseInt(employeeId) == employeeTaxRecord.getEmployeeId()) {
				result = true;
			}
		}
		return result;
	}
	
	
	// Method to add the employee tax record to the employee tax report file
	public static void writeEmployeeTaxRecordToFile(EmployeeTaxRecord employeeTaxRecord, String fileLocation) {
		int employeeId = employeeTaxRecord.getEmployeeId();
		BigDecimal taxableIncome = employeeTaxRecord.getTaxableIncome();
		BigDecimal tax = employeeTaxRecord.getTax();
		try {
			FileOutputStream taxReport = new FileOutputStream( new File(fileLocation), true);
			PrintWriter printWriter = new PrintWriter(taxReport);
			printWriter.println(employeeId + "\t\t\t" + taxableIncome + "\t\t\t" + tax);
			printWriter.close();
		} catch (FileNotFoundException e) {
			String newLocation = promptForNewFileLocation(fileLocation);
			writeEmployeeTaxRecordToFile(employeeTaxRecord, newLocation);
		}
	}
	
	
	// Method to handle asking the user if they want to add another employee tax record
	public static void promptUsertoAddAnotherEmployee(Scanner userInput, ArrayList<TaxRate> taxRates, ArrayList<EmployeeTaxRecord> employeeTaxRecordsToCheck) {
		String userResponse = "";
		while(!userResponse.equals("1") || !userResponse.equals("2")) {
			System.out.println("Would you like to calculate the tax for another employee?");
			System.out.println("1. Add another employee");
			System.out.println("2. Return to main menu");
			userResponse = userInput.next();
			if(userResponse.equals("1")) {
				getNewEmployeeTaxDetails(userInput, taxRates, employeeTaxRecordsToCheck);
			} else if (userResponse.equals("2")){
				runMenu(userInput);
			}
		}
	}
	
	
	public static void displayMenu() {
		System.out.println("Welcome to Tax Management System of XYZ\n");
		System.out.println("Please select one of the following options:");
		System.out.println("1. Calculate tax");
		System.out.println("2. Search tax");
		System.out.println("3. Exit");
	}
	
	
	public static EmployeeTaxRecord parseTaxReportLine(String taxReportLine) {
		String result = taxReportLine.replaceAll("\\s+", " ");
		String[] employeeDetails = result.split(" ");
		int employeeId = Integer.parseInt(employeeDetails[0]);
		BigDecimal taxableIncome = new BigDecimal(employeeDetails[1]);
		BigDecimal tax = new BigDecimal(employeeDetails[2]);
		EmployeeTaxRecord employeeTaxRecord = new EmployeeTaxRecord(employeeId, taxableIncome, tax);
		return employeeTaxRecord;
	}
	
	
	public static void printEmployeeTaxRecord(EmployeeTaxRecord employeeTaxRecord) {
		System.out.println("\nEmployeeID: " + employeeTaxRecord.getEmployeeId());
		System.out.println("Taxable Income: " + employeeTaxRecord.getTaxableIncome());
		System.out.println("Tax: " + employeeTaxRecord.getTax());
		System.out.println("\n");
	}
	
	// Change the import file method to use the scanner class
	@SuppressWarnings("unchecked")
	public static <T> ArrayList<T> getDataFromFile(String fileLocation){
		ArrayList<Object> dataFromFile = new ArrayList<Object>();
		try {
			File file = new File(fileLocation);
			Scanner fileInput = new Scanner(file);
			String fileLine;
			int lineCounter = 0;
			if(fileLocation.contains(taxRatesFileLocation)) {
				while(fileInput.hasNextLine()) {
					fileLine = fileInput.nextLine();
					if(lineCounter != 0) {
						// Create an instance of the TaxRate class for each of the rows in the file
						TaxRate taxRate = parseTaxRateLine(fileLine);
						dataFromFile.add(taxRate);
					}
					lineCounter++;
				}
			} else if(fileLocation.contains(taxReportFileLocation)) {				
				while(fileInput.hasNextLine()) {
					fileLine = fileInput.nextLine();
					if(lineCounter != 0) {
						// Create an instance of the TaxRate class for each of the rows in the file
						EmployeeTaxRecord employeeTaxRecord = parseTaxReportLine(fileLine);
						dataFromFile.add(employeeTaxRecord);
					}
					lineCounter++;
				}
			}
			fileInput.close();
		} catch (FileNotFoundException e) {
			String newLocation = promptForNewFileLocation(fileLocation);
			dataFromFile = getDataFromFile(newLocation);
		}
		return (ArrayList<T>) dataFromFile;
	}
	
	
	public static String promptForNewFileLocation(String fileLocation) {
		// If application is unable to locate default text file, user will be prompted to input the location of the file
		Scanner userInput = new Scanner(System.in);
		System.out.println("Unable to locate " + fileLocation);
		String inputFileLocation = "";
		if(fileLocation.contains(taxRatesFileLocation)) {
			System.out.print("Please enter the location of the tax rates file: ");
			inputFileLocation = userInput.next();	
			taxRatesFileLocation = inputFileLocation;
		} else if (fileLocation.contains(taxReportFileLocation)) {
			System.out.print("Please enter the location of the tax report file: ");
			inputFileLocation = userInput.next();
			taxReportFileLocation = inputFileLocation;
		}
		
		// Tax rates file name variable check to confirm that it has txt on the end
		if(!inputFileLocation.contains(".txt")) {
			System.out.println("Please enter the path with .txt on the end");
			inputFileLocation = promptForNewFileLocation(inputFileLocation);
		}
		return inputFileLocation;
	}
	
	
	public static TaxRate parseTaxRateLine(String taxRateLine) {
		taxRateLine = taxRateLine.replaceAll("[^\\sc0-9._-]", "");
		taxRateLine = taxRateLine.replaceAll("(\\Dc)", "");
		taxRateLine = taxRateLine.replaceAll("\t", " ");
		taxRateLine = taxRateLine.replaceAll("\\s+", " ");
		String[] lineSplit = taxRateLine.split(" ");
		
		int taxableIncomeLow = Integer.parseInt(lineSplit[0]);
		int taxableIncomeHigh = Integer.parseInt(lineSplit[1]);
		int staticTaxAmount = 0;
		int taxPerAmount = 0;
		double taxRateCents = 0;
		if(lineSplit.length > 3 ) {
			if(Integer.parseInt(lineSplit[0]) > Integer.parseInt(lineSplit[1])) {
				taxableIncomeHigh = 0;
				staticTaxAmount = Integer.parseInt(lineSplit[1]);				
			} else if (lineSplit.length == 5 && lineSplit[2].contains("c")) {
				taxRateCents = Double.parseDouble(lineSplit[2].replace("c", ""));
				taxPerAmount = Integer.parseInt(lineSplit[3]);
			} else {
				staticTaxAmount = Integer.parseInt(lineSplit[2]);
				taxPerAmount = Integer.parseInt(lineSplit[4]);
				taxRateCents = Double.parseDouble(lineSplit[3].replace("c", ""));
			}
		}
		
		TaxRate taxRate = new TaxRate(taxableIncomeLow, taxableIncomeHigh, staticTaxAmount, taxPerAmount, taxRateCents);
		return taxRate;
	}
	
	public static void printTaxRates(ArrayList<TaxRate> taxRates) {
		for(TaxRate taxRate : taxRates) {
			System.out.println("\ntaxableIncomeLow: " + taxRate.getTaxableIncomeLow());
			System.out.println("taxableIncomeHigh: " + taxRate.getTaxableIncomeHigh());
			System.out.println("staticTaxAmount: " + taxRate.getStaticTaxAmount());
			System.out.println("taxRateCents: " + taxRate.getTaxRateCents());
			System.out.println("taxRate: " + taxRate.getTaxPerAmount());
		}
	}
	
	
	public static BigDecimal calculateTax(ArrayList<TaxRate> taxRates, double annualIncome) {
		BigDecimal result = new BigDecimal(0.0);
		for(TaxRate taxRate : taxRates) {
			int lowRange = taxRate.getTaxableIncomeLow();
			int highRange = taxRate.getTaxableIncomeHigh();
			double taxRateCents = taxRate.getTaxRateCents() * 0.01;
			boolean standardTaxRange = annualIncome >= lowRange && annualIncome <= highRange;
			boolean finalTaxRange = highRange == 0 && annualIncome > lowRange;
			if(standardTaxRange || finalTaxRange) {
				result = BigDecimal.valueOf(taxRate.getStaticTaxAmount() + (annualIncome - (lowRange - 1)) * taxRateCents);
				System.out.println("Tax: " + result);
			}
		}
		return result;
	}
}
