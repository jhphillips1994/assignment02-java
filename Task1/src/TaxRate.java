
public class TaxRate {
	private int taxableIncomeLow;
	private int taxableIncomeHigh;
	private int staticTaxAmount;
	private int taxPerAmount;
	private double taxRateCents;

	public TaxRate () {
		this.taxableIncomeLow = 0;
		this.taxableIncomeHigh = 0;
		this.staticTaxAmount = 0;
		this.taxPerAmount = 0;
		this.taxRateCents = 0;
	}
	
	public TaxRate (int taxableIncomeLow,
					int taxableIncomeHigh,
					int staticTaxAmount,
					int taxPerAmount,
					double taxRateCents) {
		this.taxableIncomeLow = taxableIncomeLow;
		this.taxableIncomeHigh = taxableIncomeHigh;
		this.staticTaxAmount = staticTaxAmount;
		this.taxPerAmount = taxPerAmount;
		this.taxRateCents = taxRateCents;
	}
	
	public int getTaxableIncomeLow() {
		return taxableIncomeLow;
	}
	
	public int getTaxableIncomeHigh() {
		return taxableIncomeHigh;
	}
	
	public int getStaticTaxAmount() {
		return staticTaxAmount;
	}
	
	public int getTaxPerAmount() {
		return taxPerAmount;
	}
	
	public double getTaxRateCents() {
		return taxRateCents;
	}
	
	public void setTaxableIncomeLow(int taxableIncomeLow) {
		this.taxableIncomeLow = taxableIncomeLow;
	}
	
	public void setTaxableIncomeHigh(int taxableIncomeHigh) {
		this.taxableIncomeHigh = taxableIncomeHigh;
	}
	
	public void setStaticTaxAmount(int staticTaxAmount) {
		this.staticTaxAmount = staticTaxAmount;
	}
	
	public void setTaxPerAmount(int taxPerAmount) {
		this.taxPerAmount = taxPerAmount;
	}
	
	public void setTaxRateCents(double taxRateCents) {
		this.taxRateCents = taxRateCents;
	}
}
