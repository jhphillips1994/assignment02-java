
public class DataPoint {
	private double xCoordinate;
	private double yCoordinate;
	private String cluster;
	
	
	public DataPoint() {
		this.xCoordinate = 0;
		this.yCoordinate = 0;
		this.cluster = "null";
	}
	
	
	public DataPoint(double xCoordinate, double yCoordinate, String cluster) {
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		this.cluster = cluster;
	}
	
	
	public double getxCoordinate() {
		return xCoordinate;
	}
	
	
	public void setxCoordinate(double xCoordinate) {
		this.xCoordinate = xCoordinate;
	}
	
	
	public double getyCoordinate() {
		return yCoordinate;
	}
	
	
	public void setyCoordinate(double yCoordinate) {
		this.yCoordinate = yCoordinate;
	}
	
	
	public String getCluster() {
		return cluster;
	}
	
	
	public void setCluster(String cluster) {
		this.cluster = cluster;
	}
	
	

}
